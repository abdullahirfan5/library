package UnitTests;

import junit.framework.*;
import org.junit.Test;
import org.junit.Before;

import Classes.Cd;
import Classes.Item;
import Classes.ItemType;

import Classes.Library;
import Classes.MediaType;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.Date;



public class UnitTests {
    
    Library library;

    @Before
    public void setUp(){
        library = new Library();
    }

    @Test
    public void testLibraryInventory(){
        int cdQuantity = library.getItemQuantity(ItemType.CD);
        assertEquals(cdQuantity, 0);

        int dvdQuantity = library.getItemQuantity(ItemType.CD);
        assertEquals(dvdQuantity, 0);
        
        int bookQuantity = library.getItemQuantity(ItemType.CD);
        assertEquals(bookQuantity, 0);
        
        int periodicalQuantity = library.getItemQuantity(ItemType.CD);
        assertEquals(periodicalQuantity, 0);
    }

    @Test
    public void testCreateCd() {

        String title = "Jumanji";
        Date dateAdded = new Date();
        long itemNumber = 1234;
        MediaType mediaType = MediaType.Movie;
        Date publishDate = new Date(1999, 1, 1);
        double size = 3.0;

        Cd cd = new Cd(title, dateAdded, itemNumber, publishDate, mediaType, size);
       
        
        assertEquals(cd.getTitle(), title);
        assertEquals(cd.getDateAdded(), dateAdded);
        assertEquals(cd.getItemNumber(), itemNumber);
        assertEquals(cd.getItemType(), ItemType.CD);
        assertEquals(cd.getPublishDate(), publishDate);
        assertEquals(cd.getMediaType(), mediaType);
        assertEquals(cd.getSize(), size, 0.01);
        assertNull(cd.getBorrower());
        assertEquals(cd.getDaysRemaining(), 0);
        assertNull(cd.getIssueDate());
        assertEquals(cd.isIssued(), false);
        assertEquals(cd.isOverdue(), false);

    }

    @Test
    public void fakeAdd() {
        int sum = 1 + 3;
        assertEquals(sum, 3);
 
     }

}