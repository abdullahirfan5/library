package Classes;

import java.util.Date;

public final class Dvd extends Multimedia {

    private static final ItemType ITEMTYPE = ItemType.DVD;

    public Dvd(String title, Date dateAdded, Date publishDate, long itemNumber, MediaType mediaType, double size) {
        super(ITEMTYPE, title, dateAdded, publishDate, itemNumber, mediaType, size);
        // TODO Auto-generated constructor stub
    }

}