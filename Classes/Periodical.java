package Classes;

import java.util.Date;

public final class Periodical extends Item {
    
    private static final ItemType ITEMTYPE = ItemType.Periodical;

    public Periodical(String title, Date dateAdded, Date publishDate, long itemNumber){
        super(ITEMTYPE, title, dateAdded, publishDate, itemNumber);
    }

}