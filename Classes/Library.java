package Classes;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class Library {
    
    private HashMap<ItemType, ArrayList<Item>> inventory;
    private ArrayList<Borrower> borrowers;

    public Library(){
        super();

        inventory = new HashMap<ItemType, ArrayList<Item>>();
        inventory.put(ItemType.CD, new ArrayList<Item>());
        inventory.put(ItemType.DVD, new ArrayList<Item>());
        inventory.put(ItemType.Book, new ArrayList<Item>());
        inventory.put(ItemType.Periodical, new ArrayList<Item>());

        borrowers = new ArrayList<Borrower>();

    }

    public boolean addItem(Item item) {
        ItemType itemType = item.getItemType();
        item.setDateAdded(new Date());
        return this.inventory.get(itemType).add(item);
    }

    public boolean addBorrower(Borrower borrower){
        return borrowers.add(borrower);
    }

    public int getItemQuantity(ItemType itemType){
        return inventory.get(itemType).size();
    }

    public boolean removeItem(ItemType itemType, long itemNumber){
        
        for (Item item : inventory.get(itemType)){
            if (item.getItemNumber() == itemNumber){
                return inventory.get(itemType).remove(item);
            }
        }

        return false;

    }

    public ArrayList<Item> getItemList(ItemType itemType){
        return this.inventory.get(itemType);
    }

}