package Classes;

public enum ItemType {
    CD,
    DVD,
    Book,
    Periodical
}