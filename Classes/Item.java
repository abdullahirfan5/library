package Classes;

import java.util.Date;

public abstract class Item {
    
    private ItemType itemType;
    private String title;
    private Date dateAdded;
    private Date publishDate;
    private long itemNumber;

    public Item(){
        super();
    }

    public Item(ItemType itemType, String title, Date dateAdded, Date publishDate, long itemNumber){
        this.itemType = itemType;
        this.title = title;
        this.dateAdded = dateAdded;
        this.publishDate = publishDate;
        this.itemNumber = itemNumber;
    }

    public ItemType getItemType() {
        return itemType;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(Date dateAdded) {
        this.dateAdded = dateAdded;
    }

    public long getItemNumber() {
        return itemNumber;
    }

    public void setItemNumber(long itemNumber) {
        this.itemNumber = itemNumber;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    @Override
    public String toString() {
        return "Item [dateAdded=" + dateAdded + ", itemNumber=" + itemNumber + ", itemType=" + itemType
                + ", publishDate=" + publishDate + ", title=" + title + "]";
    }

}