package Classes;

import java.util.Date;

public abstract class Borrowable extends Item {

    private boolean isIssued;
    private Date issueDate;
    private int daysRemaining;
    private boolean isOverdue;
    private Borrower borrower;

    public Borrowable(ItemType itemType, String title, Date dateAdded, Date publishDate, long itemNumber){
        super(itemType, title, dateAdded, publishDate, itemNumber);
        this.isIssued = false;
        this.issueDate = null;
        this.daysRemaining = 0;
        this.isOverdue = false;
        this.borrower = null;
    }

    public boolean isIssued() {
        return isIssued;
    }

    public void setIssued(boolean isIssued) {
        this.isIssued = isIssued;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public int getDaysRemaining() {
        return daysRemaining;
    }

    public void setDaysRemaining(int daysRemaining) {
        this.daysRemaining = daysRemaining;
    }

    public boolean isOverdue() {
        return isOverdue;
    }

    public void setOverdue(boolean isOverdue) {
        this.isOverdue = isOverdue;
    }

    public void returnItem(){
        this.setIssued(false);
        this.setIssueDate(null);
        this.setDaysRemaining(0);
        this.setOverdue(false);

        this.getBorrower().returnItem(this);

        this.setBorrower(null);

    }

    public void issueItem(Borrower borrower, int daysRemaining){
        this.setIssued(true);

        // setting current date as issue date
        this.setIssueDate(new Date());

        borrower.borrowItem(this);

        this.setBorrower(borrower);

        this.setDaysRemaining(daysRemaining);

        this.setOverdue(false);
    }

    public Borrower getBorrower() {
        return borrower;
    }

    public void setBorrower(Borrower borrower) {
        this.borrower = borrower;
    }

}