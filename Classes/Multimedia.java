package Classes;

import java.util.Date;

public abstract class Multimedia extends Borrowable {

    private MediaType mediaType;
    private double size;

    public Multimedia(ItemType itemType, String title, Date dateAdded, Date publishDate, long itemNumber, MediaType mediaType, double size){

        super(itemType, title, dateAdded, publishDate, itemNumber);
        this.mediaType = mediaType;
        this.size = size;

    }

    public MediaType getMediaType() {
        return mediaType;
    }

    public void setMediaType(MediaType mediaType) {
        this.mediaType = mediaType;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

}