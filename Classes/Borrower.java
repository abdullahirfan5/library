package Classes;

import java.util.ArrayList;

public class Borrower {

    private String name;
    private long contactNum;
    private String email;
    private ArrayList<Borrowable> borrowedItems;

    public Borrower(String name, long contactNum, String email){
        this.name = name;
        this.contactNum = contactNum;
        this.email = email;
        this.borrowedItems = new ArrayList<Borrowable>();
    }

    public boolean returnItem(Borrowable borrowable) {
        return borrowedItems.remove(borrowable);
    }

    public boolean borrowItem(Borrowable borrowable){
        return borrowedItems.add(borrowable);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getContactNum() {
        return contactNum;
    }

    public void setContactNum(long contactNum) {
        this.contactNum = contactNum;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public ArrayList<Borrowable> getBorrowedItems() {
        return borrowedItems;
    }

    public void setBorrowedItems(ArrayList<Borrowable> borrowedItems) {
        this.borrowedItems = borrowedItems;
    }

}