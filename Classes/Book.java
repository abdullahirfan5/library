package Classes;

import java.util.Date;

public final class Book extends Borrowable {

    private static final ItemType ITEMTYPE = ItemType.Book;

    public Book(String title, Date dateAdded, Date publishDate, long itemNumber) {
        super(ITEMTYPE, title, dateAdded, publishDate, itemNumber);
        // TODO Auto-generated constructor stub
    }
    
}